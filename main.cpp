#include <iostream>
#include <dlfcn.h>

using ADD_FUNC=int(*)(int,int);

int main () {
	void *handle = dlopen("./libfoo.so", RTLD_NOW);
	if (!handle) {
		std::cerr << dlerror() << std::endl;
		return -1;
	}

	ADD_FUNC add = (ADD_FUNC) dlsym(handle, "add");
	if (!add) {
		std::cerr << dlerror() << std::endl;
		return -1;
	}

	std::cout << add(1,1) << std::endl;

	dlclose(handle);

	std::cout << "hello world" << std::endl;
	return 0;
}
